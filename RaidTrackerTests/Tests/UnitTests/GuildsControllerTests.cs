﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using RaidTrackerAuth.Core.Controllers;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using Xunit;

namespace RaidTrackerTests.Tests.UnitTests
{
    public class GuildsControllerTests
    {
        private IEnumerable<Guild> GetTestGuilds()
        {
            var guilds = new List<Guild>
            {
                new Guild()
                {
                    Id = 1,
                    Name = "TestName1"
                },
                new Guild()
                {
                    Id = 2,
                    Name = "TestName2"
                },
                new Guild()
                {
                    Id = 3,
                    Name = "TestName3"
                }
            };

            return guilds;
        }

        /* GET GUILDS */
        [Fact]
        public async Task GetGuilds_Test()
        {
            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetAll())
                .Returns(Task.FromResult(GetTestGuilds()));
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetGuilds();

            Assert.Equal(3, result.Count());
        }

        /* GET GUILD */
        [Fact]
        public async Task GetGuild_BadRequestObjectResult_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            var controller = new GuildsController(mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var result = await controller.GetGuild(testId);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task GetGuild_NotFoundResult_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().SingleOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.Get(testId))
                .ReturnsAsync(testGuild);
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetGuild(testId + 1);

            Assert.NotNull(result);
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GetGuild_OkObjectResult_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().SingleOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.Get(testId))
                .ReturnsAsync(testGuild);
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetGuild(testId);

            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
        }

        /* POST GUILD */
        [Fact]
        public async Task PostGuild_CreatedAtAction_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().FirstOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.Add(testGuild))
                .Returns(Task.CompletedTask).Verifiable();
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.PostGuild(testGuild);

            var createdResult = Assert.IsType<CreatedAtActionResult>(result);
            var returnGuild = Assert.IsType<Guild>(createdResult.Value);

            Assert.Equal(testId, returnGuild.Id);
            Assert.Equal("TestName1", returnGuild.Name);
        }

        [Fact]
        public async Task PostGuild_BadRequest_Test()
        {
            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.Add(new Guild())).Returns(Task.CompletedTask);
            var controller = new GuildsController(mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var result = await controller.PostGuild(new Guild());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        /* PUT GUILD */
        [Fact]
        public async Task PutGuild_BadRequestObjectResult_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            var controller = new GuildsController(mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var result = await controller.PutGuild(testId, new Guild());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task PutGuild_BadRequest_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.PutGuild(testId, new Guild());

            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task PutGuild_NoContentResult_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().FirstOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.SetModified(testGuild));
            mockRepo.Setup(repo => repo.SaveChangesAsync()).Returns(Task.FromResult((int)testId));
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.PutGuild(testId, testGuild);

            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task PutGuild_Exception_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().FirstOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.SetModified(testGuild));
            mockRepo.Setup(repo => repo.SaveChangesAsync()).Throws(new Exception());

            var controller = new GuildsController(mockRepo.Object);

            await Assert.ThrowsAsync<Exception>(() => controller.PutGuild(testId, testGuild));
        }

        /* GET GUILD BY NAME */
        [Fact]
        public async Task GetGuildByName_BadRequestObjectResult_Test()
        {
            var mockRepo = new Mock<IUnitOfWork>();
            var controller = new GuildsController(mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var result = await controller.GetGuildByName("");

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task GetGuildByName_NotFoundResult_Test()
        {
            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetByName("")).ReturnsAsync((Guild) null);
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetGuildByName("");

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GetGuildByName_OkObjectResult_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().FirstOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetByName(testGuild.Name)).ReturnsAsync(testGuild);
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetGuildByName(testGuild.Name);

            Assert.IsType<OkObjectResult>(result);
        }

        /* GET USER GUILDS */
        [Fact]
        public async Task GetUserGuilds_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetUserGuilds(testId))
                .Returns(Task.FromResult(GetTestGuilds()));
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.GetUserGuilds(testId);

            Assert.Equal(3, result.Count());
        }

        /* DELETE GUILD */
        [Fact]
        public async Task DeleteGuild_BadRequestObjectResult_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            var controller = new GuildsController(mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var result = await controller.DeleteGuild(testId);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task DeleteGuild_NotFoundResult_Test()
        {
            long testId = 1;

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetSingleOrDefaultAsync(g => g.Id == testId)).ReturnsAsync((Guild) null);
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.DeleteGuild(testId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task DeleteGuild_OkObjectResult_Test()
        {
            long testId = 1;
            var testGuild = GetTestGuilds().FirstOrDefault(g => g.Id == testId);

            var mockRepo = new Mock<IUnitOfWork>();
            mockRepo.Setup(repo => repo.Guilds.GetSingleOrDefaultAsync(It.IsAny<Expression<Func<Guild, bool>>>())).ReturnsAsync(testGuild);
            mockRepo.Setup(repo => repo.Guilds.Remove(testGuild));
            mockRepo.Setup(repo => repo.SaveChangesAsync()).Returns(Task.FromResult((int)testId));
            var controller = new GuildsController(mockRepo.Object);

            var result = await controller.DeleteGuild(testId);

            Assert.IsType<OkObjectResult>(result);
        }
    }
}
