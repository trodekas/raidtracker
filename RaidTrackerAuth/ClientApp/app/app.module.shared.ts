import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AlertModule } from 'ngx-bootstrap/alert';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from "./components/profile/profile.component";
import { GuildComponent } from "./components/guild/guild.component";
import { GuildModalComponent } from "./components/guildmodal/guildmodal.component";
import { GuildFormComponent } from "./components/guildform/guildform.component";
import { EventComponent } from "./components/event/event.component";
import { EventModalComponent } from "./components/eventmodal/eventmodal.component";
import { EventFormComponent } from "./components/eventform/eventform.component";

import { AuthService } from "./services/auth.service";
import { AuthGuardService } from "./services/auth-guard.service";
import { UserService } from "./services/user.service";
import { FooterComponent } from "./components/footer/footer.component";
import { GuildService } from "./services/guild.service";
import { EventService } from "./services/event.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TimepickerModule } from "ngx-bootstrap/timepicker";

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
    return new AuthHttp(new AuthConfig({
        tokenGetter: (() => localStorage.getItem('access_token'))
    }), http, options);
}

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        ProfileComponent,
        FooterComponent,
        GuildComponent,
        GuildModalComponent,
        GuildFormComponent,
        EventComponent,
        EventModalComponent,
        EventFormComponent
    ],
    entryComponents: [
        GuildModalComponent,
        EventModalComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ModalModule.forRoot(),
        CarouselModule.forRoot(),  
        AlertModule.forRoot(), 
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'guild', component: GuildComponent, canActivate: [AuthGuardService] },
            { path: 'guild-form', component: GuildFormComponent, canActivate: [AuthGuardService] },
            { path: 'event', component: EventComponent, canActivate: [AuthGuardService] },
            { path: 'event-form', component: EventFormComponent, canActivate: [AuthGuardService] },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
        AuthService,
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        },
        AuthGuardService,
        UserService,
        GuildService,
        EventService,
        BsModalService
    ]
})
export class AppModuleShared {
}
