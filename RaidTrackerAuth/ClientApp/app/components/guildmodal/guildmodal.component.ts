﻿import { Component } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal";

@Component({
    selector: 'guildmodal',
    templateUrl: './guildmodal.component.html'   
})

export class GuildModalComponent {
    title: string;
    members: any[] = [];
    constructor(public bsModalRef: BsModalRef) { }
}