﻿import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from "../../models/user.model";

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

    profile: any;

    constructor(public auth: AuthService) { }

    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
        } else {
            this.auth.getFullProfile((err, profile) => {
                this.profile = profile;
            });
        }
    }

}