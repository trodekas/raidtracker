﻿import { Component } from '@angular/core';
import { GuildEvent } from "../../models/guildevent.model";
import { EventService } from "../../services/event.service";
import { AuthService } from "../../services/auth.service";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule, TimepickerConfig } from 'ngx-bootstrap/timepicker';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Guild } from "../../models/guild.model";
import { GuildService } from "../../services/guild.service";

export function getTimepickerConfig(): TimepickerConfig {
    return Object.assign(new TimepickerConfig(), {
        minuteStep: 10,
        showMeridian: false,
        readonlyInput: false,
        mousewheel: true
    });
}

@Component({
    selector: 'eventform',
    templateUrl: './eventform.component.html',
    styleUrls: ['./eventform.component.css'],
    providers: [{ provide: TimepickerConfig, useFactory: getTimepickerConfig }]
})

export class EventFormComponent {
    successHidden = true;
    saveDisabled = false;

    guilds: Guild[];

    minDate;
    bsConfig: Partial<BsDatepickerConfig>;

    event: GuildEvent =
    {
        id: 0,
        name: '',
        summary: '',
        startTime: null,
        guild: null,
        participants: []
    };
    startDate: Date;
    startTime: Date;

    eventDate: Date = new Date();

    constructor(private eventService: EventService, private auth: AuthService, private guildService: GuildService) {
        this.minDate = Date.now();
        this.bsConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });

        this.guildService.getUserGuilds(this.auth.loggedUser.id).subscribe(res => this.guilds = res);
    }

    submit() {
        this.eventDate.setFullYear(this.startDate.getFullYear());
        this.eventDate.setMonth(this.startDate.getMonth());
        this.eventDate.setDate(this.startDate.getDate());

        this.eventDate.setHours(this.startTime.getHours());
        this.eventDate.setMinutes(this.startTime.getMinutes());

        this.event.startTime = this.eventDate;

        var result$ = this.eventService.addEvent(this.event);
        result$.subscribe(res => { this.saveDisabled = true; this.successHidden = false });
    }
}