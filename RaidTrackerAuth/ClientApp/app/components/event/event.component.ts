﻿import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { EventService } from "../../services/event.service";
import { GuildEvent } from "../../models/guildevent.model";
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from "ngx-bootstrap/modal";
import { EventModalComponent } from "../eventmodal/eventmodal.component";

@Component({
    selector: 'event',
    templateUrl: './event.component.html'
})
export class EventComponent implements OnInit {
    public events: GuildEvent[];
    bsModalRef: BsModalRef;

    constructor(private eventService: EventService, private auth: AuthService, private modalService: BsModalService) {

    }

    ngOnInit() {
        this.eventService.getEventsForUser(this.auth.loggedUser.id).subscribe(result => this.events = result);
    }

    viewEventDetails(i) {
        this.bsModalRef = this.modalService.show(EventModalComponent);
        this.bsModalRef.content.title = this.events[i].name;
        this.bsModalRef.content.summary = this.events[i].summary;
        this.bsModalRef.content.participants = this.events[i].participants;
        this.bsModalRef.content.eventId = this.events[i].id;
        this.bsModalRef.content.userId = this.auth.loggedUser.id;
        this.bsModalRef.content.canJoin = this.setCanJoin(i);

        this.modalService.onHide.subscribe(res => this.eventService.getEventsForUser(this.auth.loggedUser.id).subscribe(result => this.events = result));
    }

    setCanJoin(i) {
        for (let p of this.events[i].participants) {
            if (p.userId == this.auth.loggedUser.id) {
                return false;
            }
        }

        return true;
    }
}