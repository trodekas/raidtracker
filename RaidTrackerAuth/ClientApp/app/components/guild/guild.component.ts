﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Guild } from "../../models/guild.model";
import { GuildService } from "../../services/guild.service";
import { AuthService } from "../../services/auth.service";
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from "ngx-bootstrap/modal";
import { GuildModalComponent } from "../guildmodal/guildmodal.component";

@Component({
    selector: 'guild',
    templateUrl: './guild.component.html'
})
export class GuildComponent implements OnInit {
    public guilds: Guild[];
    bsModalRef: BsModalRef;

    constructor(private guildService: GuildService, private auth: AuthService, private modalService: BsModalService)
    {
       
    }

    ngOnInit()
    {
        this.guildService.getUserGuilds(this.auth.loggedUser.id).subscribe(result => this.guilds = result);
    }

    viewGuildDetails(i)
    {
        this.bsModalRef = this.modalService.show(GuildModalComponent);
        this.bsModalRef.content.title = this.guilds[i].name;
        this.bsModalRef.content.members = this.guilds[i].members;
    }
}