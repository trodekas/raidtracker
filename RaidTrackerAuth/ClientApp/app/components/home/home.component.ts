import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";
import { User } from "../../models/user.model";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    providers: [
        { provide: CarouselConfig, useValue: { interval: 5000, noPause: false } }
    ]
})
export class HomeComponent implements OnInit
{
    userEmail: any;

    user: User =
    {
        id: null,
        email: ''
    };

    constructor(private auth: AuthService, private userService: UserService)
    {

    }

    ngOnInit() {
        this.auth.userEmailChange$.subscribe(email => this.handleEmailChange(email));
    }

    handleEmailChange(email)
    {
        this.userEmail = email;
        this.userService.getUser(this.userEmail).subscribe(res => this.handleFoundUser(res), error => this.registerUser());
    }

    handleFoundUser(res)
    {
        this.auth.loggedUser = res;
    }

    registerUser()
    {
        this.user.email = this.userEmail;
        this.userService.createNewUser(this.user).subscribe(res => this.auth.loggedUser = res);
    }
}
