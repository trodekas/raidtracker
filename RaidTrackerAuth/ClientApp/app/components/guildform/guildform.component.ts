﻿import { Component, OnInit } from '@angular/core';
import { Guild } from "../../models/guild.model";
import { GuildService } from "../../services/guild.service";
import { AuthService } from "../../services/auth.service";
import { GuildMember } from "../../models/guildmember.model";

@Component({
    selector: 'guildform',
    templateUrl: './guildform.component.html',
    styleUrls: ['./guildform.component.css']
})
export class GuildFormComponent{
    successHidden = true;
    errorHidden = true;
    saveDisabled = false;

    guild: Guild =
    {
        id: 0,
        name: '',
        events: [],
        members: []
    };

    guildCreator: GuildMember =
    {
        id: 0,
        rank: 'Leader',
        guildId: null,
        userId: null
    }

    constructor(private guildService: GuildService, private auth: AuthService) {

    }

    submit() {
        this.errorHidden = true;

        this.guildService.getGuildByName(this.guild.name)
            .subscribe(res => this.errorHidden = false, err => this.createGuild());
    }

    createGuild() {
        var result$ = this.guildService.createGuild(this.guild);
        result$.subscribe(guild => {
            this.guildCreator.userId = this.auth.loggedUser.id;
            this.guildCreator.guildId = guild.id;

            this.guildService.addMember(this.guildCreator).subscribe(guildMember => {
                this.successHidden = false;
                this.saveDisabled = true;
            });
        });
    }
}