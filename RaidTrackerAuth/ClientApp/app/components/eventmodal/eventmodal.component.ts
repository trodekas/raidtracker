﻿import { Component } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal";
import { EventParticipant } from "../../models/eventparticipant.model";
import { EventService } from "../../services/event.service";

@Component({
    selector: 'eventmodal',
    templateUrl: './eventmodal.component.html'
})

export class EventModalComponent {
    title: string;
    summary: string;
    participants: any[] = [];
    eventId: number = 0;
    userId: number = 0;
    canJoin: boolean = true;

    eventParticipant: EventParticipant =
    {
        id: 0,
        isBackup: false,
        eventId: null,
        userId: null
    };

    constructor(public bsModalRef: BsModalRef, private eventService: EventService) { }

    joinEvent() {
        this.eventParticipant.eventId = this.eventId;
        this.eventParticipant.userId = this.userId;

        this.eventService.addEventParticipant(this.eventParticipant).subscribe(res => this.bsModalRef.hide());
    }
}