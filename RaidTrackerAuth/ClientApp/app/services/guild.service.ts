﻿import { Injectable, OnInit } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Guild } from "../models/guild.model";
import { AuthService } from "./auth.service";
import { Http } from "@angular/http";

@Injectable()
export class GuildService
{
    private readonly guildEndpoint = '/api/guilds';
    private readonly guildMemberEndpoint = '/api/guildmembers';

    constructor(private authHttp: AuthHttp)
    {

    }

    getAllGuilds()
    {
        return this.authHttp.get(this.guildEndpoint)
            .map(res => res.json());
    }

    getGuildByName(name) {
        return this.authHttp.get(this.guildEndpoint + '/name/' + name)
            .map(res => res.json());
    }

    getUserGuilds(id)
    {
        return this.authHttp.get(this.guildEndpoint + '/user/' + id)
            .map(res => res.json());
    }

    createGuild(guild)
    {
        return this.authHttp.post(this.guildEndpoint, guild)
            .map(res => res.json());
    }

    addMember(member)
    {
        return this.authHttp.post(this.guildMemberEndpoint, member)
            .map(res => res.json());
    }
}