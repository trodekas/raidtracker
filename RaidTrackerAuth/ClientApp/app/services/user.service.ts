﻿import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { User } from "../models/user.model";
import { Http } from "@angular/http";

@Injectable()
export class UserService
{
    private readonly userEndpoint = '/api/users';

    constructor(private authHttp: AuthHttp)
    {

    }

    createNewUser(user)
    {
        return this.authHttp.post(this.userEndpoint, user)
            .map(res => res.json());
    }

    getUser(email)
    {
        return this.authHttp.get(this.userEndpoint + '/' + email)
            .map(res => res.json());
    }
}