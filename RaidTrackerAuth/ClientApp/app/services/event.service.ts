﻿import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { User } from "../models/user.model";
import { Http } from "@angular/http";

@Injectable()
export class EventService {
    private readonly eventEndpoint = '/api/events';
    private readonly eventParticipantEnpoint = '/api/eventparticipants';

    constructor(private authHttp: AuthHttp) {

    }

    getEventsForUser(userId) {
        return this.authHttp.get(this.eventEndpoint + '/user/' + userId)
            .map(res => res.json());
        
    }

    addEventParticipant(participant) {
        return this.authHttp.post(this.eventParticipantEnpoint, participant)
            .map(res => res.json());
    }

    addEvent(event) {
        return this.authHttp.post(this.eventEndpoint, event)
            .map(res => res.json());
    }
}