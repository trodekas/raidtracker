﻿export interface GuildMember {
    id: number;
    rank: string;
    userId: number;
    guildId: number;
}