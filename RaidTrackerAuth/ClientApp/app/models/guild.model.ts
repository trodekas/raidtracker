﻿export interface Guild
{
    id: number;
    name: string;
    members: any[];
    events: any[];
}