﻿import { Guild } from "./guild.model";

export interface GuildEvent {
    id: number;
    name: string;
    summary: string;
    startTime: Date;
    guild: Guild;
    participants: any[];
}