﻿export interface EventParticipant {
    id: number;
    isBackup: boolean;
    userId: number;
    eventId: number;
}