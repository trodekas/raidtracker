﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace RaidTrackerAuth.Core.Controllers
{
    [Authorize]
    [Route("api/Guilds")]
    public class GuildsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public GuildsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Guilds
        [HttpGet]
        public async Task<IEnumerable<Guild>> GetGuilds()
        {
            return await _unitOfWork.Guilds.GetAll();
        }

        [HttpGet("user/{id}")]
        public async Task<IEnumerable<Guild>> GetUserGuilds([FromRoute] long id)
        {
            return await _unitOfWork.Guilds.GetUserGuilds(id);
        }

        // GET: api/Guilds/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGuild([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guild = await _unitOfWork.Guilds.Get(id);

            if (guild == null)
            {
                return NotFound();
            }

            return Ok(guild);
        }

        // GET: api/Guilds/Name/Guild1
        [HttpGet("Name/{name}")]
        public async Task<IActionResult> GetGuildByName([FromRoute] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guild = await _unitOfWork.Guilds.GetByName(name);

            if (guild == null)
            {
                return NotFound();
            }

            return Ok(guild);
        }

        // PUT: api/Guilds/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGuild([FromRoute] long id, [FromBody] Guild guild)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != guild.Id)
            {
                return BadRequest();
            }

            _unitOfWork.Guilds.SetModified(guild);

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await GuildExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Guilds
        [HttpPost]
        public async Task<IActionResult> PostGuild([FromBody] Guild guild)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _unitOfWork.Guilds.Add(guild);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction("GetGuild", new { id = guild.Id }, guild);
        }

        // DELETE: api/Guilds/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGuild([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guild = await _unitOfWork.Guilds.GetSingleOrDefaultAsync(m => m.Id == id);
            if (guild == null)
            {
                return NotFound();
            }

            _unitOfWork.Guilds.Remove(guild);
            await _unitOfWork.SaveChangesAsync();

            return Ok(guild);
        }

        private async Task<bool> GuildExists(long id)
        {
            return await _unitOfWork.Guilds.Any(e => e.Id == id);
        }
    }
}