﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace RaidTrackerAuth.Core.Controllers
{
    [Authorize]
    [Route("api/Events")]
    public class EventsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public EventsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Events
        [HttpGet]
        public async Task<IEnumerable<Event>> GetEvents()
        {
            return await _unitOfWork.Events.GetAll();
        }

        // GET: api/Events/User/5
        [HttpGet("User/{id}")]
        public async Task<IEnumerable<Event>> GetEventsForUser(long id)
        {
            return await _unitOfWork.Events.GetEventsForUser(id);
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEvent([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @event = await _unitOfWork.Events.Get(id);

            if (@event == null)
            {
                return NotFound();
            }

            return Ok(@event);
        }

        // PUT: api/Events/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEvent([FromRoute] long id, [FromBody] Event @event)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @event.Id)
            {
                return BadRequest();
            }

            _unitOfWork.Events.SetModified(@event);

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Events
        [HttpPost]
        public async Task<IActionResult> PostEvent([FromBody] Event @event)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _unitOfWork.Events.Add(@event);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction("GetEvent", new { id = @event.Id }, @event);
        }

        // DELETE: api/Events/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEvent([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @event = await _unitOfWork.Events.GetSingleOrDefaultAsync(e => e.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            _unitOfWork.Events.Remove(@event);
            await _unitOfWork.SaveChangesAsync();

            return Ok(@event);
        }

        private async Task<bool> EventExists(long id)
        {
            return await _unitOfWork.Events.Any(e => e.Id == id);
        }
    }
}