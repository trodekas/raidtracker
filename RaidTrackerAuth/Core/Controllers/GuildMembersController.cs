﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace RaidTrackerAuth.Core.Controllers
{
    [Authorize]
    [Route("api/GuildMembers")]
    public class GuildMembersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public GuildMembersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/GuildMembers
        [HttpGet]
        public async Task<IEnumerable<GuildMember>> GetGuildMembers()
        {
            return await _unitOfWork.GuildMembers.GetAll();
        }

        // GET: api/GuildMembers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGuildMember([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guildMember = await _unitOfWork.GuildMembers.Get(id);

            if (guildMember == null)
            {
                return NotFound();
            }

            return Ok(guildMember);
        }

        // PUT: api/GuildMembers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGuildMember([FromRoute] long id, [FromBody] GuildMember guildMember)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != guildMember.Id)
            {
                return BadRequest();
            }

            _unitOfWork.GuildMembers.SetModified(guildMember);

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await GuildMemberExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GuildMembers
        [HttpPost]
        public async Task<IActionResult> PostGuildMember([FromBody] GuildMember guildMember)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _unitOfWork.GuildMembers.Add(guildMember);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction("GetGuildMember", new { id = guildMember.Id }, guildMember);
        }

        // DELETE: api/GuildMembers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGuildMember([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guildMember = await _unitOfWork.GuildMembers.Get(id);
            if (guildMember == null)
            {
                return NotFound();
            }

            _unitOfWork.GuildMembers.Remove(guildMember);
            await _unitOfWork.SaveChangesAsync();

            return Ok(guildMember);
        }

        private async Task<bool> GuildMemberExists(long id)
        {
            return await _unitOfWork.GuildMembers.Any(e => e.Id == id);
        }
    }
}