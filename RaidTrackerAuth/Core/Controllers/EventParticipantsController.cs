﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace RaidTrackerAuth.Core.Controllers
{
    [Authorize]
    [Route("api/EventParticipants")]
    public class EventParticipantsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public EventParticipantsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/EventParticipants
        [HttpGet]
        public async Task<IEnumerable<EventParticipant>> GetEventParticipants()
        {
            return await _unitOfWork.EventParticipants.GetAll();
        }

        // GET: api/EventParticipants/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEventParticipant([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var eventParticipant = await _unitOfWork.EventParticipants.Get(id);

            if (eventParticipant == null)
            {
                return NotFound();
            }

            return Ok(eventParticipant);
        }

        // PUT: api/EventParticipants/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEventParticipant([FromRoute] long id, [FromBody] EventParticipant eventParticipant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eventParticipant.Id)
            {
                return BadRequest();
            }

            _unitOfWork.EventParticipants.SetModified(eventParticipant);

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await EventParticipantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EventParticipants
        [HttpPost]
        public async Task<IActionResult> PostEventParticipant([FromBody] EventParticipant eventParticipant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _unitOfWork.EventParticipants.Add(eventParticipant);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction("GetEventParticipant", new { id = eventParticipant.Id }, eventParticipant);
        }

        // DELETE: api/EventParticipants/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEventParticipant([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var eventParticipant = await _unitOfWork.EventParticipants.Get(id);
            if (eventParticipant == null)
            {
                return NotFound();
            }

            _unitOfWork.EventParticipants.Remove(eventParticipant);
            await _unitOfWork.SaveChangesAsync();

            return Ok(eventParticipant);
        }

        private async Task<bool> EventParticipantExists(long id)
        {
            return await _unitOfWork.EventParticipants.Any(e => e.Id == id);
        }
    }
}