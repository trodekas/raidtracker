﻿namespace RaidTrackerAuth.Core.Models
{
    public class EventParticipant
    {
        public long Id { get; set; }
        public bool IsBackup { get; set; }
        
        // Navigation properties
        public long EventId { get; set; }
        public Event Event { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }
}
