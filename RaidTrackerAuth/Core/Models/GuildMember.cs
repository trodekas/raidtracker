﻿namespace RaidTrackerAuth.Core.Models
{
    public class GuildMember
    {            
        public long Id { get; set; }
        public string Rank { get; set; }

        // Navigation properties
        public long GuildId { get; set; }
        public Guild Guild { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }
}
