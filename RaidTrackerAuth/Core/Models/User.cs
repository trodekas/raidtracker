﻿using System.Collections.Generic;

namespace RaidTrackerAuth.Core.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; }

        // Navigation properties
        public ICollection<EventParticipant> EventParticipant { get; set; }
        public ICollection<GuildMember> GuildMember { get; set; }
    }
}
