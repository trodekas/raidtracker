﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RaidTrackerAuth.Core.Models
{
    public class Event
    {
        public long Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        public string Summary { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartTime { get; set; }

        // Navigation properties
        public long GuildId { get; set; }
        public Guild Guild { get; set; }
        public ICollection<EventParticipant> Participants { get; set; }
    }
}
