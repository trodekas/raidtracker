﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RaidTrackerAuth.Core.Models
{
    public class Guild
    {
        public long? Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        // Navigation properties
        public ICollection<Event> Events { get; set; }
        public ICollection<GuildMember> Members { get; set; }
    }
}
