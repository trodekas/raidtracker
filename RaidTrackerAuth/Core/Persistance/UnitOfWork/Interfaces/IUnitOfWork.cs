﻿using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        IGuildRepository Guilds { get; }
        IEventRepository Events { get; }
        IEventParticipantRepository EventParticipants { get; }
        IGuildMemberRepository GuildMembers { get; }

        Task<int> SaveChangesAsync();
    }
}
