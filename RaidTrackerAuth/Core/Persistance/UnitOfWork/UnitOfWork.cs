﻿using RaidTrackerAuth.Core.Persistance.Repositories;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using RaidTrackerAuth.Core.Persistance.UnitOfWork.Interfaces;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly RaidTrackerDbContext _context;

        IUserRepository _users;
        IGuildRepository _guilds;
        IEventRepository _events;
        IEventParticipantRepository _eventParticipants;
        IGuildMemberRepository _guildMembers;

        public UnitOfWork(RaidTrackerDbContext context)
        {
            _context = context;
        }

        public IUserRepository Users
        {
            get
            {
                if(_users == null)
                {
                    _users = new UserRepository(_context);
                }

                return _users;
            }
        }

        public IGuildRepository Guilds
        {
            get
            {
                if(_guilds == null)
                {
                    _guilds = new GuildRepository(_context);
                }

                return _guilds;
            }
        }

        public IEventRepository Events
        {
            get
            {
                if(_events == null)
                {
                    _events = new EventRepository(_context);
                }

                return _events;
            }
        }

        public IEventParticipantRepository EventParticipants
        {
            get
            {
                if (_eventParticipants == null)
                {
                    _eventParticipants = new EventParticipantRepository(_context);
                }

                return _eventParticipants;
            }
        }

        public IGuildMemberRepository GuildMembers
        {
            get
            {
                if (_guildMembers == null)
                {
                    _guildMembers = new GuildMemberRepository(_context);
                }

                return _guildMembers;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
