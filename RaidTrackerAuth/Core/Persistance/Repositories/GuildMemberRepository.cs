﻿using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace RaidTrackerAuth.Core.Persistance.Repositories
{
    public class GuildMemberRepository : Repository<GuildMember>, IGuildMemberRepository
    {
        private RaidTrackerDbContext Context => (RaidTrackerDbContext)_context;

        public GuildMemberRepository(DbContext context) : base(context)
        {

        }
    }
}
