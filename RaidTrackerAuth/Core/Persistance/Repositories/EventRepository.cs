﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;

namespace RaidTrackerAuth.Core.Persistance.Repositories
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        private RaidTrackerDbContext Context => (RaidTrackerDbContext)_context;

        public EventRepository(RaidTrackerDbContext context) : base(context)
        {
            
        }

        public async Task<IEnumerable<Event>> GetEventsForUser(long id)
        {
            return await Context.Events
                .Include(e => e.Guild)
                .Include(e => e.Participants)
                .ThenInclude(p => p.User)
                .Where(e => e.Guild.Members.Any(m => m.UserId == id)).ToListAsync();
        }

    }
}
