﻿using RaidTrackerAuth.Core.Models;
using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Repositories
{
    public class GuildRepository : Repository<Guild>, IGuildRepository
    {
        private RaidTrackerDbContext Context => (RaidTrackerDbContext)_context;

        public GuildRepository(RaidTrackerDbContext context) : base(context)
        {
           
        }

        public override async Task<Guild> Get(params object[] ids)
        {
            return await Context.Guilds
                .Include(g => g.Members).ThenInclude(m => m.User)
                .Include(g => g.Events)
                .SingleOrDefaultAsync(g => g.Id == (long)ids.First());
        }

        public override async Task<IEnumerable<Guild>> GetAll()
        {
            return await Context.Guilds
                .Include(g => g.Members).ThenInclude(m => m.User)
                .Include(g => g.Events).ToListAsync();
        }

        public async Task<IEnumerable<Guild>> GetUserGuilds(long id)
        {
            return await Context.Guilds
                .Include(g => g.Members).ThenInclude(m => m.User)
                .Include(g => g.Events)
                .Where(g => g.Members.Any(m => m.UserId == id)).ToListAsync();
        }

        public async Task<Guild> GetByName(string name)
        {
            return await Context.Guilds.SingleAsync(g => g.Name == name);
        }
    }
}
