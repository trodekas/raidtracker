﻿using RaidTrackerAuth.Core.Models;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetUserByEmail(string email);
    }
}
