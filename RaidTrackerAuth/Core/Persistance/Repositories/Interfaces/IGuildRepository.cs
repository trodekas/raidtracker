﻿using RaidTrackerAuth.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Repositories.Interfaces
{
    public interface IGuildRepository : IRepository<Guild>
    {
        Task<IEnumerable<Guild>> GetUserGuilds(long id);
        Task<Guild> GetByName(string name);
    }
}
