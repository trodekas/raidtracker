﻿using RaidTrackerAuth.Core.Models;

namespace RaidTrackerAuth.Core.Persistance.Repositories.Interfaces
{
    public interface IEventParticipantRepository : IRepository<EventParticipant>
    {

    }
}
