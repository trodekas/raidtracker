﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RaidTrackerAuth.Core.Models;

namespace RaidTrackerAuth.Core.Persistance.Repositories.Interfaces
{
    public interface IEventRepository : IRepository<Event>
    {
        Task<IEnumerable<Event>> GetEventsForUser(long id);
    }
}
