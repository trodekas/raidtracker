﻿using RaidTrackerAuth.Core.Models;

namespace RaidTrackerAuth.Core.Persistance.Repositories.Interfaces
{
    public interface IGuildMemberRepository : IRepository<GuildMember>
    {
    }
}
