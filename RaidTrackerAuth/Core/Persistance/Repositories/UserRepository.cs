﻿using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private RaidTrackerDbContext Context => (RaidTrackerDbContext)_context;

        public UserRepository(RaidTrackerDbContext context) : base(context)
        {
            
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await Context.Users.FirstOrDefaultAsync(user => user.Email == email);
        }
    }
}
