﻿using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace RaidTrackerAuth.Core.Persistance.Repositories
{
    public class EventParticipantRepository : Repository<EventParticipant>, IEventParticipantRepository
    {
        private RaidTrackerDbContext Context => (RaidTrackerDbContext)_context;

        public EventParticipantRepository(DbContext context) : base(context)
        {

        }
    }
}
