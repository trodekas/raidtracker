﻿using Microsoft.EntityFrameworkCore;
using RaidTrackerAuth.Core.Models;

namespace RaidTrackerAuth.Core.Persistance
{
    public class RaidTrackerDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Guild> Guilds { get; set; }
        public DbSet<GuildMember> GuildMembers { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventParticipant> EventParticipants { get; set; }

        public RaidTrackerDbContext(DbContextOptions<RaidTrackerDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}
