﻿using RaidTrackerAuth.Core.Models;
using RaidTrackerAuth.Core.Persistance.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Data
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly RaidTrackerDbContext _context;

        public DatabaseInitializer(RaidTrackerDbContext context)
        {
            _context = context;
        }

        public async Task SeedDbAsync()
        {
            // Users
            User user1 = new User() { Email = "email1" };
            User user2 = new User() { Email = "email2" };
            User user3 = new User() { Email = "email3" };
            User user4 = new User() { Email = "email4" };
            User user5 = new User() { Email = "email5" };
            User user6 = new User() { Email = "email6" };
            User user7 = new User() { Email = "email7" };
            User user8 = new User() { Email = "trodekas@gmail.com" };

            // Guilds
            Guild guild1 = new Guild()
            {
                Name = "guild1"
            };
            Guild guild2 = new Guild()
            {
                Name = "guild2"
            };

            // Events
            Event event1 = new Event()
            {
                Name = "event1",
                Summary = "summary1",
                StartTime = DateTime.Now,
                Guild = guild1
            };
            Event event2 = new Event()
            {
                Name = "event2",
                Summary = "summary2",
                StartTime = DateTime.Now,
                Guild = guild1
            };
            Event event3 = new Event()
            {
                Name = "event3",
                Summary = "summary3",
                StartTime = DateTime.Now,
                Guild = guild2
            };

            // GuildMembers
            GuildMember guildMember1 = new GuildMember()
            {
                Rank = "rank1",
                Guild = guild1,
                User = user1
            };
            GuildMember guildMember2 = new GuildMember()
            {
                Rank = "rank2",
                Guild = guild1,
                User = user2
            };
            GuildMember guildMember3 = new GuildMember()
            {
                Rank = "rank3",
                Guild = guild1,
                User = user3
            };
            GuildMember guildMember4 = new GuildMember()
            {
                Rank = "rank4",
                Guild = guild1,
                User = user8
            };

            // EventParticipants
            EventParticipant eventParticipant1 = new EventParticipant
            {
                IsBackup = false,
                User = user1,
                Event = event1
            };
            EventParticipant eventParticipant2 = new EventParticipant
            {
                IsBackup = false,
                User = user2,
                Event = event1
            };
            EventParticipant eventParticipant3 = new EventParticipant
            {
                IsBackup = true,
                User = user3,
                Event = event1
            };
            EventParticipant eventParticipant4 = new EventParticipant
            {
                IsBackup = true,
                User = user8,
                Event = event1
            };

            // Users events
            user1.EventParticipant = new List<EventParticipant> { eventParticipant1 };
            user2.EventParticipant = new List<EventParticipant> { eventParticipant2 };
            user3.EventParticipant = new List<EventParticipant> { eventParticipant3 };
            user8.EventParticipant = new List<EventParticipant> { eventParticipant4 };

            // Users guilds
            user1.GuildMember = new List<GuildMember> { guildMember1 };
            user2.GuildMember = new List<GuildMember> { guildMember2 };
            user3.GuildMember = new List<GuildMember> { guildMember3 };
            user8.GuildMember = new List<GuildMember> { guildMember4 };

            // Guild members
            guild1.Members = new List<GuildMember> { guildMember1, guildMember2, guildMember3, guildMember4 };

            // Guild events
            guild1.Events = new List<Event> { event1, event2 };
            guild2.Events = new List<Event> { event3 };

            // Event partcipants
            event1.Participants = new List<EventParticipant> { eventParticipant1, eventParticipant2, eventParticipant3, eventParticipant4 };

            // Add to database
            await _context.Users.AddAsync(user1);
            await _context.Users.AddAsync(user2);
            await _context.Users.AddAsync(user3);
            await _context.Users.AddAsync(user4);
            await _context.Users.AddAsync(user5);
            await _context.Users.AddAsync(user6);
            await _context.Users.AddAsync(user7);
            await _context.Users.AddAsync(user8);

            await _context.Guilds.AddAsync(guild1);
            await _context.Guilds.AddAsync(guild2);

            await _context.Events.AddAsync(event1);
            await _context.Events.AddAsync(event2);

            await _context.GuildMembers.AddAsync(guildMember1);
            await _context.GuildMembers.AddAsync(guildMember2);
            await _context.GuildMembers.AddAsync(guildMember3);
            await _context.GuildMembers.AddAsync(guildMember4);

            await _context.EventParticipants.AddAsync(eventParticipant1);
            await _context.EventParticipants.AddAsync(eventParticipant2);
            await _context.EventParticipants.AddAsync(eventParticipant3);

            await _context.SaveChangesAsync();
        }
    }
}
