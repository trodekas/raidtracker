﻿using System.Threading.Tasks;

namespace RaidTrackerAuth.Core.Persistance.Data.Interfaces
{
    interface IDatabaseInitializer
    {
        Task SeedDbAsync();
    }
}
